{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](/) - [Useful concepts and STL](/notebooks/5-UsefulConceptsAndSTL/0-main.ipynb) - [Algorithms](/notebooks/5-UsefulConceptsAndSTL/7-Algorithms.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction\" data-toc-modified-id=\"Introduction-1\">Introduction</a></span></li><li><span><a href=\"#Example:-std::sort\" data-toc-modified-id=\"Example:-std::sort-2\">Example: <code>std::sort</code></a></span></li><li><span><a href=\"#std::find\" data-toc-modified-id=\"std::find-3\"><code>std::find</code></a></span></li><li><span><a href=\"#Output-iterators-and-std::back_inserter\" data-toc-modified-id=\"Output-iterators-and-std::back_inserter-4\">Output iterators and <code>std::back_inserter</code></a></span></li><li><span><a href=\"#The-different-kinds-of-operators\" data-toc-modified-id=\"The-different-kinds-of-operators-5\">The different kinds of operators</a></span></li><li><span><a href=\"#Algorithm:-read-the-documentation-first!\" data-toc-modified-id=\"Algorithm:-read-the-documentation-first!-6\">Algorithm: read the documentation first!</a></span><ul class=\"toc-item\"><li><span><a href=\"#std::unique\" data-toc-modified-id=\"std::unique-6.1\">std::unique</a></span></li><li><span><a href=\"#std::remove\" data-toc-modified-id=\"std::remove-6.2\">std::remove</a></span></li></ul></li><li><span><a href=\"#Conclusion\" data-toc-modified-id=\"Conclusion-7\">Conclusion</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even if C++ can't be qualified as a _batteries included_ language like Python (until C++ 17 there was no proper filesystem management, and even now the support of this feature is shaky at best in several STL implementations...), there are plenty of algorithms that are already provided within the STL.\n",
    "\n",
    "We won't obviously list them all here - the mighty \\cite{Josuttis2012} which is more than 1000 pages long don't do it either! - but show few examples on how to use them. For instance, many STL algorithms rely upon iterators: this way a same algorithm may be used as well on `std::vector`, `std::list`, and so on...\n",
    "\n",
    "A side note: if a STL class provides a method which as a namesake algorithm, use the method. For instance there is a `std::sort` algorithm, but `std::list` provides a method which takes advantage on the underlying structure of the object and is therefore much more efficient.\n",
    "\n",
    "## Example: `std::sort`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <algorithm>\n",
    "#include <vector>\n",
    "#include <deque>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100 };\n",
    "    std::deque<double> double_deque { -9., 87., 11., 0., -21., 100. };\n",
    "    \n",
    "    std::sort(int_vec.begin(), int_vec.end());\n",
    "    std::sort(double_deque.begin(), double_deque.end(), std::greater<double>());    \n",
    "    \n",
    "    for (auto item : int_vec)\n",
    "        std::cout << item << \" \";\n",
    "    std::cout << std::endl;\n",
    "\n",
    "    for (auto item : double_deque)\n",
    "        std::cout << item << \" \";\n",
    "    std::cout << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, the same algorithm works upon two different types of objects. It works with non constant iterators; an optional third argument to `std::sort` enables to provide your own sorting algorithm.\n",
    "\n",
    "Lambda functions may be used as well to provide the comparison to use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <algorithm>\n",
    "#include <vector>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100 };\n",
    "    \n",
    "    std::sort(int_vec.begin(), int_vec.end(), \n",
    "              [](auto lhs, auto rhs)\n",
    "              {\n",
    "                  const bool is_lhs_even = (lhs % 2 == 0);\n",
    "                  const bool is_rhs_even = (rhs % 2 == 0);                  \n",
    "\n",
    "                  // Even must be ordered first, then odds\n",
    "                  // Granted, this is really an oddball choice..\n",
    "                  if (is_lhs_even && !is_rhs_even)\n",
    "                      return true;\n",
    "                  \n",
    "                  if (is_rhs_even && !is_lhs_even)\n",
    "                      return false;\n",
    "                  \n",
    "                    return lhs < rhs;\n",
    "              });\n",
    "    \n",
    "    for (auto item : int_vec)\n",
    "        std::cout << item << \" \";\n",
    "    std::cout << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, we may use this on something other than `begin()` and `end()`; we just have to make sure iterators are valid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <algorithm>\n",
    "#include <vector>\n",
    "#include <iostream>\n",
    "#include <cassert>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100 };\n",
    "    \n",
    "    auto it = int_vec.begin() + 4;\n",
    "    assert(it < int_vec.end()); // Important condition to check iterator means something!\n",
    "    \n",
    "    std::sort(int_vec.begin(), it); // Only first four elements are sort.\n",
    "    \n",
    "    for (auto item : int_vec)\n",
    "        std::cout << item << \" \";\n",
    "    std::cout << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `std::find`\n",
    "\n",
    "I will also show examples of `std::find` as it provides an additional common practice: it returns an iterator, and there is a specific behaviour if the algorithm failed to find something.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21 };\n",
    "    \n",
    "    const auto it = std::find(int_vec.cbegin(), int_vec.cend(), -21);\n",
    "    \n",
    "    if (it != int_vec.cend())\n",
    "        std::cout << \"Found at position \" << it - int_vec.cbegin() << std::endl;        \n",
    "    else\n",
    "        std::cout << \"Not found.\" << std::endl;\n",
    "   \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, `std::find` returns the first instance in the iterator range. You may know how many instances there are with `std::count`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 };\n",
    "    \n",
    "    const auto count = std::count(int_vec.cbegin(), int_vec.cend(), -21);\n",
    "    \n",
    "    std::cout << \"There are \" << count << \" instances of -21.\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to use a condition rather than a value, there are dedicated versions of the algorithms to do so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 };\n",
    "    \n",
    "    const auto count = std::count_if(int_vec.cbegin(), int_vec.cend(),\n",
    "                                     [](int value)\n",
    "                                        {\n",
    "                                            return value % 2 == 0;\n",
    "                                        });\n",
    "    \n",
    "    std::cout << \"There are \" << count << \" even values in the list.\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output iterators and `std::back_inserter`\n",
    "\n",
    "Some algorithms require output iterators: they don't work uniquely upon existing content but need to shove new data somewhere. You must in this case provide the adequate memory beforehand:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 }; \n",
    "    std::vector<int> odd_only;\n",
    "    \n",
    "    std::copy_if(int_vec.cbegin(), int_vec.cend(),\n",
    "                 odd_only.begin(),\n",
    "                  [](int value)\n",
    "                  {\n",
    "                      return value % 2 != 0;\n",
    "                  }                                     \n",
    "                 ); // SHOULD MAKE YOUR KERNEL CRASH!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The issue is that the memory is not allocated first: the algorithm doesn't provide the memory at destination! \n",
    "\n",
    "Of course, in some cases it is tricky to know in advance what you need, and here computing it previously with `std::count_if` add an additional operation. There is actually a way to tell the program to insert the values by `push_back` with `std::back_inserter`; it might be a good idea to reserve enough memory to use this method without recopy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 }; \n",
    "    std::vector<int> odd_only;\n",
    "    odd_only.reserve(int_vec.size()); // at most all elements of int_vec will be there\n",
    "    \n",
    "    std::copy_if(int_vec.cbegin(), int_vec.cend(),\n",
    "                 std::back_inserter(odd_only),\n",
    "                 [](int value)\n",
    "                 {\n",
    "                     return value % 2 != 0;\n",
    "                 }                                     \n",
    "                ); \n",
    "    \n",
    "    // And if you're afraid to have used to much memory with your `reserve()` call, \n",
    "    // you may call shrink_to_fit() method here.\n",
    "    \n",
    "    std::cout << \"The odd values are: \";\n",
    "    \n",
    "    for (auto item : odd_only)\n",
    "        std::cout << item << \" \";\n",
    "    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The different kinds of operators\n",
    "\n",
    "`std::back_inserter` works only with containers that provide a `push_back()` method. This may be generalized: the fact that algorithms rely upon iterators to make them as generic as possible doesn't mean each algorithm will work on any container. \n",
    "\n",
    "There are actually several kinds of iterators:\n",
    "\n",
    "* **Forward iterators**, which which you may only iterate forward. For instance `std::forward_list` or `std::unordered_map` provide such iterators.\n",
    "* **Bidirectional iterators**, which way you may also iterate backward. For instance `std::list` or `std::map` provide such iterators.\n",
    "* **Random-access iterators**, which are bidirectional operators with on top of it the ability to provide random access (through an index). Think of `std::vector` or `std::string`.\n",
    "\n",
    "When you go on [cppreference](https://en.cppreference.com/w/) (or in \\cite{Josuttis2012}) the name of the template parameter explicitly describes which kind of iterator is actually used.\n",
    "\n",
    "Besides this classification, there are also in algorithms the difference between **input iterators** (which are read-only) and **output iterators** that assume you will write new content there.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algorithm: read the documentation first!\n",
    "\n",
    "You should really **carefully read the documentation** before using an algorithm: it might not behave as you believe...\n",
    "\n",
    "I will provide two examples:\n",
    "\n",
    "### std::unique\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 }; \n",
    "   \n",
    "    std::unique(int_vec.begin(), int_vec.end());\n",
    "       \n",
    "    std::cout << \"The unique values are (or not...): \";\n",
    "    \n",
    "    for (auto item : int_vec)\n",
    "        std::cout << item << \" \";\n",
    "    \n",
    "    std::cout << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So what's happen? If you look at [cppreference](http://www.cplusplus.com/reference/algorithm/unique/) you may see the headline is _Remove **consecutive** duplicates in range_.\n",
    "\n",
    "So to make it work you need to sort it first (or use a home-made algorithm if you need to preserve the original ordering):\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 }; \n",
    "   \n",
    "    std::sort(int_vec.begin(), int_vec.end());\n",
    "    std::unique(int_vec.begin(), int_vec.end());\n",
    "       \n",
    "    std::cout << \"The unique values are (really this time): \";\n",
    "    \n",
    "    for (auto item : int_vec)\n",
    "        std::cout << item << \" \";\n",
    "    \n",
    "    std::cout << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Personally I have in my Utilities library a function `EliminateDuplicate()` which calls both in a row."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### std::remove"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 }; \n",
    "   \n",
    "    std::remove_if(int_vec.begin(), int_vec.end(),\n",
    "                  [](int value)\n",
    "                   { \n",
    "                       return value % 2 != 0;\n",
    "                   });\n",
    "    \n",
    "    std::cout << \"The even values are (or not...): \";\n",
    "    \n",
    "    for (auto item : int_vec)\n",
    "        std::cout << item << \" \";\n",
    "    \n",
    "    std::cout << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So what happens this time? [cppreference](http://www.cplusplus.com/reference/algorithm/remove/?kw=remove) tells that it _transforms the range \\[first,last) into a range with all the elements that compare equal to val removed, and returns an iterator to the new end of that range_.\n",
    "\n",
    "In other words, `std::remove`:\n",
    "\n",
    "* Place at the beginning of the vector the values to be kept.\n",
    "* Returns an iterator to the **logical end** of the expected series...\n",
    "* But does not deallocate the memory! (and keeps the container `size()` - see below)\n",
    "\n",
    "So to print the relevant values only, you should do:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 }; \n",
    "   \n",
    "    auto logical_end = std::remove_if(int_vec.begin(), int_vec.end(),\n",
    "                                      [](int value)\n",
    "                                      { \n",
    "                                          return value % 2 != 0;\n",
    "                                      });\n",
    "    \n",
    "    std::cout << \"The even values are: \";\n",
    "    \n",
    "    for (auto it = int_vec.cbegin(); it != logical_end; ++it)\n",
    "        std::cout << *it << \" \";\n",
    "    \n",
    "    std::cout << std::endl;\n",
    "    std::cout << \"But the size of the vector is still \" << int_vec.size() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And if you want to reduce this size, you should use the `std::vector::erase()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <algorithm>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<int> int_vec { -9, 87, 11, 0, -21, 100, -21, 17, -21 }; \n",
    "   \n",
    "    auto logical_end = std::remove_if(int_vec.begin(), int_vec.end(),\n",
    "                                      [](int value)\n",
    "                                      { \n",
    "                                          return value % 2 != 0;\n",
    "                                      });\n",
    "    \n",
    "    int_vec.erase(logical_end, int_vec.end());\n",
    "    \n",
    "    std::cout << \"The even values are: \";\n",
    "    \n",
    "    for (auto item : int_vec)\n",
    "        std::cout << item << \" \";\n",
    "    \n",
    "    std::cout << std::endl;\n",
    "    std::cout << \"And the size of the vector is correctly \" << int_vec.size() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "My point was absolutely not to tell you not to use the STL algorithms; on the contrary it is better not to reinvent the wheel, especially considering you would likely end up with a less efficient version of the algorithm!\n",
    "\n",
    "You need however to be very careful: sometimes the names are unfortunately misleading, and you should always check a function does the job you have in mind.\n",
    "\n",
    "I have barely scratched the surface; many algorithms are extremely useful. So whenever you want to proceed with a transformation that is likely common (check a range is sorted, partition a list in a specific way, finding minimum and maximum, etc...) it is highly likely the STL has something in store for you.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "[<a id=\"cit-Josuttis2012\" href=\"#call-Josuttis2012\">Josuttis2012</a>] Nicolai M. Josuttis, ``_The C++ Standard Library: A Tutorial and Reference_'',  2012.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "© _CNRS 2016_ - _Inria 2018-2019_   \n",
    "_This notebook is an adaptation of a lecture prepared by David Chamont (CNRS) under the terms of the licence [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/)_  \n",
    "_The present version has been written by Sébastien Gilles and Vincent Rouvreau (Inria)_"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xeus-cling-cpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "-std=c++17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "key",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
