{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](/) - [Procedural programming](/notebooks/1-ProceduralProgramming/0-main.ipynb) - [Predefined types](/notebooks/1-ProceduralProgramming/1-Variables.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Ordinary-variables\" data-toc-modified-id=\"Ordinary-variables-1\">Ordinary variables</a></span><ul class=\"toc-item\"><li><span><a href=\"#Declaration\" data-toc-modified-id=\"Declaration-1.1\">Declaration</a></span></li><li><span><a href=\"#Initialisation\" data-toc-modified-id=\"Initialisation-1.2\">Initialisation</a></span></li><li><span><a href=\"#Affectation\" data-toc-modified-id=\"Affectation-1.3\">Affectation</a></span></li><li><span><a href=\"#Increment-and-decrement-operators\" data-toc-modified-id=\"Increment-and-decrement-operators-1.4\">Increment and decrement operators</a></span></li><li><span><a href=\"#Comparing-values\" data-toc-modified-id=\"Comparing-values-1.5\">Comparing values</a></span></li></ul></li><li><span><a href=\"#References\" data-toc-modified-id=\"References-2\">References</a></span></li><li><span><a href=\"#Pointers\" data-toc-modified-id=\"Pointers-3\">Pointers</a></span><ul class=\"toc-item\"><li><span><a href=\"#nullptr\" data-toc-modified-id=\"nullptr-3.1\"><code>nullptr</code></a></span></li></ul></li><li><span><a href=\"#Constant-variables-and-pointers\" data-toc-modified-id=\"Constant-variables-and-pointers-4\">Constant variables and pointers</a></span></li><li><span><a href=\"#Arrays\" data-toc-modified-id=\"Arrays-5\">Arrays</a></span><ul class=\"toc-item\"><li><span><a href=\"#Arrays-and-pointers\" data-toc-modified-id=\"Arrays-and-pointers-5.1\">Arrays and pointers</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ordinary variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Declaration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To be usable in a C++ program, a variable must be declared. This declaration shall include at least the type of\n",
    "the variable, followed by its name and a semicolon."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "32766\n",
      "1.37345e-70\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    int number; // integer variable\n",
    "    double real;  // floating-point variable\n",
    "\n",
    "    std::cout << number << std::endl; \n",
    "    std::cout << real << std::endl;\n",
    "}\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialisation\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Although not mandatory, it is **strongly** recommended to give a\n",
    "initial value to your variables, as an expression between brackets. \n",
    "\n",
    "Not providing an initial value may lead to unexpected behaviour. For instance you can't make hypothesis upon the values of `number` and `real` in the cell above: you might end-up with any value... and someone else might get other values on his computer!\n",
    "\n",
    "\n",
    "If you give braces without values, a predefined and associated value\n",
    "to the type is used (usually a form of 0)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    int nb1 { 1 }; // integer variable set with the value 1\n",
    "    int nb2 {}; // same as int nb2{0};\n",
    "    double pi { 3.14 };  // real variable\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "C++ actually supports many other historical forms\n",
    "of initialization, which you will encounter everywhere, including in this tutorial,\n",
    "with brackets and/or an equal sign. There are some subtle differences\n",
    "between each other... that you can ignore most of the time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    int a = 5;\n",
    "    int b(5);\n",
    "    int c = { 5 };\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In all cases, even if there is an equal sign, it is important to remember\n",
    "that it is an initialization, not an assignment (this will be\n",
    "important when we will define our own types)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Affectation\n",
    "\n",
    "A new value is stored in an existing variable using the operator\n",
    "of assignment `=`. The name of the variable is on the left; the expression\n",
    "on the right of the `=` sign is evaluated, and its result is assigned to the variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Default initialization: a = 0, b = 0 and c = 0\n",
      "After affectations: a = 4, b = 7 and c = 11\n"
     ]
    }
   ],
   "source": [
    "#include <iostream> // for std::cout and std::endl\n",
    "\n",
    "{\n",
    "    int a {}, b {}, c {} ;  // default initialization; set the values to 0\n",
    "    \n",
    "    std::cout << \"Default initialization: a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "\n",
    "    a = 4;\n",
    "    b = 7;\n",
    "    c = a + b;\n",
    "    \n",
    "    std::cout << \"After affectations: a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "    \n",
    "}   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Affectations may be chained:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "a = 5, b = 5 and c = 5\n"
     ]
    }
   ],
   "source": [
    "{\n",
    "    int a {}, b {}, c {};\n",
    "    \n",
    "    a = b = c = 5;    \n",
    "    \n",
    "    std::cout << \"a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to define (slightly) more advanced operators of assignments that modify the value currently stored by a simple operation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "a = 4\n",
      "a = 28\n",
      "a = 5\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    int a {}, b {}, c {} ;  // default initialization; set the values to 0\n",
    "    \n",
    "    a += 4; // add 4 to the current value of a\n",
    "    std::cout << \"a = \" << a << std::endl;\n",
    "    a *= 7; // multiply current value of a by 3\n",
    "    std::cout << \"a = \" << a << std::endl;\n",
    "    a /= 5; // divide a by 5 and assign the quotient to a\n",
    "    std::cout << \"a = \" << a << std::endl;\n",
    "    \n",
    "}   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Increment and decrement operators\n",
    "\n",
    "Finally, C++ also provides a shortcut when value is either incremented or decremented by adding `++` or `--` before or after the name of the variable. \n",
    "\n",
    "* If the sign is placed before the variable, it is a **pre-increment**. \n",
    "* If the sign is placed after the variable, it is a **post-increment**. \n",
    "\n",
    "An example is the better way to explain the difference between both:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "a = 7, b = 3 and c = 10\n",
      "a = 6, b = 3 and c = 10\n",
      "a = 6, b = 4 and c = 10\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    int a = 5;\n",
    "    int b = 3;\n",
    "    \n",
    "    a++; // increment a by 1.\n",
    "    ++a; // same, both are actually equivalents here.\n",
    "    \n",
    "    int c = a + b;    \n",
    "    std::cout << \"a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "    \n",
    "    c = a-- + b; // first a + b is evaluated, and only then a is decremented.    \n",
    "    std::cout << \"a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "\n",
    "    c = a + ++b; // first b is incremented, and only then a + b is evaluated.\n",
    "    std::cout << \"a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Honestly it's usually better to remove any ambiguity by separating explicitly both operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "a = 7, b = 3 and c = 10\n",
      "a = 6, b = 3 and c = 10\n",
      "a = 6, b = 4 and c = 10\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    int a = 7;\n",
    "    int b = 3;\n",
    "\n",
    "    int c = a + b;\n",
    "    std::cout << \"a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "        \n",
    "    c = a + b;\n",
    "    --a; // equivalent to a-- but for reasons related to the standard library I advise you \n",
    "         // to rather use the pre-increment form when both are equivalent.\n",
    "    std::cout << \"a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "       \n",
    "    ++b; // same: equivalent to b++;    \n",
    "    c = a + b;\n",
    "    std::cout << \"a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Comparing values\n",
    "\n",
    "As shown above, `=` is the affectation operator. To compare two values, the symbol to use is `==`.\n",
    "\n",
    "Other arithmetic operators are:\n",
    "\n",
    "| Operator      | Effect                                      |\n",
    "|:------------- |:-------------------------------------------:|\n",
    "| `a == b`      | `true` if a and b are equals                |\n",
    "| `a != b`      | `true` if a and b are different             |\n",
    "| `a < b`       | `true` if a is less than b                  |\n",
    "| `a > b`       | `true` if a is greater than b               |\n",
    "| `a >= b`       | `true` if a is greater than or equal to b  |\n",
    "| `a <= b`       | `true` if a is less than or equal to b     |\n",
    "\n",
    "\n",
    "These operators are defined for most ordinary types and may be defined for your own types (we'll see that [later](/notebooks/3-Operators/2-Comparison.ipynb)).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "\n",
    "A reference is a variable that acts as a kind of alias, and provides another name for the same value.\n",
    "\n",
    "When defining a reference, it must be immediately initialized by\n",
    "indicating on which variable it should point; it cannot be changed after that.\n",
    "\n",
    "The syntax is to add a `&` character just after the type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Initial values     : a = 2, b = 3 and c = 2\n",
      "After b is modified: a = 2, b = 8 and c = 2\n",
      "After c is modified: a = 3, b = 8 and c = 3\n",
      "After a is modified: a = 9, b = 8 and c = 9\n"
     ]
    }
   ],
   "source": [
    "#include <iostream> \n",
    "\n",
    "{\n",
    "    int a { 2 };\n",
    "    int b { a };\n",
    "    b = b + 1;\n",
    "    int& c { a }; // c is a reference to a\n",
    "\n",
    "    std::cout << \"Initial values     : a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "    b = b + 5;\n",
    "    std::cout << \"After b is modified: a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "    c = c + 1 ; //\n",
    "    std::cout << \"After c is modified: a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "    a *= 3;\n",
    "    std::cout << \"After a is modified: a = \" << a << \", b = \" << b << \" and c = \" << c << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reference is a purely C++ concept that doesn't exist in C."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pointers\n",
    "\n",
    "A pointer contains the address of another variable. It declares itself by slipping\n",
    "a `*` character before the name. It can be initialized or not with the address\n",
    "of another variable. To explicitly extract the address of this other variable,\n",
    "we use the symbol `&`.\n",
    "\n",
    "Later, either you want to modify the address stored in the pointer, to which\n",
    "in this case we use the pointer name directly, or we want to modify the variable\n",
    "pointed, in which case the name is prefixed with `*`.\n",
    "\n",
    "We can therefore see the pointer as a kind of redefinable reference; pointers are a feature from C language."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Initial values: \n",
      "\t p: address = 0x7ffeea882974, value = 2\n",
      "\t a: address = 0x7ffeea882974, value = 2\n",
      "\t b: address = 0x7ffeea882970, value = 10\n",
      "\n",
      "After value pointed by p is incremented:\n",
      "\t p: address = 0x7ffeea882974, value = 3\n",
      "\t a: address = 0x7ffeea882974, value = 3\n",
      "\t b: address = 0x7ffeea882970, value = 10\n",
      "\n",
      "p now points to the address of b:\n",
      "\t p: address = 0x7ffeea882970, value = 10\n",
      "\t a: address = 0x7ffeea882974, value = 3\n",
      "\t b: address = 0x7ffeea882970, value = 10\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{    \n",
    "    int a { 2 }, b { 10 };\n",
    "    int* p {&a}; // define a pointer p which is initialized with the address of a\n",
    "\n",
    "    std::cout << \"Initial values: \" << std::endl;\n",
    "    std::cout << \"\\t p: address = \" << p << \", value = \" << *p << std::endl;\n",
    "    std::cout << \"\\t a: address = \" << &a << \", value = \" << a << std::endl;\n",
    "    std::cout << \"\\t b: address = \" << &b << \", value = \" << b << std::endl;\n",
    "    \n",
    "    std::cout << std::endl;\n",
    "    *p = *p + 1; // increment the integer value present at address p\n",
    "    std::cout << \"After value pointed by p is incremented:\" << std::endl;\n",
    "    std::cout << \"\\t p: address = \" << p << \", value = \" << *p << std::endl;\n",
    "    std::cout << \"\\t a: address = \" << &a << \", value = \" << a << std::endl;\n",
    "    std::cout << \"\\t b: address = \" << &b << \", value = \" << b << std::endl;\n",
    "    \n",
    "    std::cout << std::endl;\n",
    "    p = &b; // change the address pointed by p\n",
    "    std::cout << \"p now points to the address of b:\" << std::endl;\n",
    "    std::cout << \"\\t p: address = \" << p << \", value = \" << *p << std::endl;\n",
    "    std::cout << \"\\t a: address = \" << &a << \", value = \" << a << std::endl;\n",
    "    std::cout << \"\\t b: address = \" << &b << \", value = \" << b << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `nullptr`\n",
    "\n",
    "A pointer can also designate no variables if initialized with the special value\n",
    " `nullptr`. It is then a mistake to try to access its pointed value (as we shall see later, an [`assert`](/notebooks/5-UsefulConceptsAndSTL/1-ErrorHandling.ipynb#Assert) is a good idea to ensure we do not try to dereference a `nullptr` value).\n",
    "\n",
    "It is strongly recommended to initialize a pointer when creating it: if you define an uninitialized pointer, it points to an arbitrary area of memory, which can create undefined behaviors that are not necessarily reproducible.\n",
    "\n",
    "If the pointed area is known at initialization and never changes throughout the program, you should consider a reference rather than a pointer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\t p: address = 0x0, value = "
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[1minput_line_24:4:62: \u001b[0m\u001b[0;1;35mwarning: \u001b[0m\u001b[1mnull passed to a callee that requires a non-null argument [-Wnonnull]\u001b[0m\n",
      "    std::cout << \"\\t p: address = \" << p << \", value = \" << *p << std::endl; // Dereferencing p is misguided!\n",
      "\u001b[0;1;32m                                                             ^\n",
      "\u001b[0m"
     ]
    },
    {
     "ename": "Interpreter Exception",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "Interpreter Exception: "
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{    \n",
    "    int* p = nullptr; // define a null pointer p\n",
    "\n",
    "    std::cout << \"\\t p: address = \" << p << \", value = \" << *p << std::endl; // Dereferencing p is misguided!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constant variables and pointers\n",
    "\n",
    "When declaring a variable of a predefined type, it is possible to specify its value can't be changed afterward by using the word `const` which may be placed before or after the type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[1minput_line_23:6:8: \u001b[0m\u001b[0;1;31merror: \u001b[0m\u001b[1mcannot assign to variable 'pi' with const-qualified type 'const double'\u001b[0m\n",
      "    pi = 5.; // COMPILATION ERROR!\n",
      "\u001b[0;1;32m    ~~ ^\n",
      "\u001b[0m\u001b[1minput_line_23:3:18: \u001b[0m\u001b[0;1;30mnote: \u001b[0mvariable 'pi' declared const here\u001b[0m\n",
      "    const double pi = 3.1415927;\n",
      "\u001b[0;1;32m    ~~~~~~~~~~~~~^~~~~~~~~~~~~~\n",
      "\u001b[0m\u001b[1minput_line_23:7:10: \u001b[0m\u001b[0;1;31merror: \u001b[0m\u001b[1mcannot assign to variable 'pi_2' with const-qualified type 'const double'\u001b[0m\n",
      "    pi_2 = 7.; // COMPILATION ERROR!\n",
      "\u001b[0;1;32m    ~~~~ ^\n",
      "\u001b[0m\u001b[1minput_line_23:4:18: \u001b[0m\u001b[0;1;30mnote: \u001b[0mvariable 'pi_2' declared const here\u001b[0m\n",
      "    double const pi_2 = 3.1415927; // equally valid; it is just a matter of taste. Mine is to put it before,\n",
      "\u001b[0;1;32m    ~~~~~~~~~~~~~^~~~~~~~~~~~~~~~\n",
      "\u001b[0m"
     ]
    },
    {
     "ename": "Interpreter Error",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "Interpreter Error: "
     ]
    }
   ],
   "source": [
    "{\n",
    "    const double pi = 3.1415927;\n",
    "    double const pi_2 = 3.1415927; // equally valid; it is just a matter of taste. Mine is to put it before,\n",
    "                                   // so that is what you will see in the remaining of the lecture.\n",
    "    pi = 5.; // COMPILATION ERROR!\n",
    "    pi_2 = 7.; // COMPILATION ERROR!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the case of a pointer, we can declare the pointer itself constant, and/or the value pointed to depending on whether we place the keyword `const` before or after the type name (in this case it applies to the pointed value) or after the character `*` (in this case it applies to the pointer itself):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    int a { 2 }, b { 3 };\n",
    "    int* p { &a }; // Both pointer and pointed values are modifiable.\n",
    "    \n",
    "    p = &b; // OK\n",
    "    *p = 5; // OK     \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[1minput_line_27:6:7: \u001b[0m\u001b[0;1;31merror: \u001b[0m\u001b[1mcannot assign to variable 'p' with const-qualified type 'int *const'\u001b[0m\n",
      "    p = &b; // COMPILATION ERROR - if you comment it it will \n",
      "\u001b[0;1;32m    ~ ^\n",
      "\u001b[0m\u001b[1minput_line_27:4:16: \u001b[0m\u001b[0;1;30mnote: \u001b[0mvariable 'p' declared const here\u001b[0m\n",
      "    int* const p { &a }; // Value is modifiable, but not the address pointed to.\n",
      "\u001b[0;1;32m    ~~~~~~~~~~~^~~~~~~~\n",
      "\u001b[0m"
     ]
    },
    {
     "ename": "Interpreter Error",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "Interpreter Error: "
     ]
    }
   ],
   "source": [
    "{\n",
    "    int a { 2 }, b { 3 };\n",
    "    int* const p { &a }; // Value is modifiable, but not the address pointed to.\n",
    "\n",
    "    *p = 5; // OK     \n",
    "    p = &b; // COMPILATION ERROR - if you comment it it will \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[1minput_line_28:7:8: \u001b[0m\u001b[0;1;31merror: \u001b[0m\u001b[1mread-only variable is not assignable\u001b[0m\n",
      "    *p = 5; // COMPILATION ERROR    \n",
      "\u001b[0;1;32m    ~~ ^\n",
      "\u001b[0m"
     ]
    },
    {
     "ename": "Interpreter Error",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "Interpreter Error: "
     ]
    }
   ],
   "source": [
    "{\n",
    "    int a { 2 }, b { 3 };\n",
    "    const int* p { &a }; // Address pointed to is modifiable, but not the underlying value.\n",
    "    \n",
    "    p = &b; // OK\n",
    "    *p = 5; // COMPILATION ERROR    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[1minput_line_76:6:7: \u001b[0m\u001b[0;1;31merror: \u001b[0m\u001b[1mcannot assign to variable 'p' with const-qualified type 'const int *const'\u001b[0m\n",
      "    p = &b; // COMPILATION ERROR\n",
      "\u001b[0;1;32m    ~ ^\n",
      "\u001b[0m\u001b[1minput_line_76:4:22: \u001b[0m\u001b[0;1;30mnote: \u001b[0mvariable 'p' declared const here\u001b[0m\n",
      "    const int* const p { &a }; // Nothing is modifiable\n",
      "\u001b[0;1;32m    ~~~~~~~~~~~~~~~~~^~~~~~~~\n",
      "\u001b[0m\u001b[1minput_line_76:7:8: \u001b[0m\u001b[0;1;31merror: \u001b[0m\u001b[1mread-only variable is not assignable\u001b[0m\n",
      "    *p = 5; // COMPILATION ERROR \n",
      "\u001b[0;1;32m    ~~ ^\n",
      "\u001b[0m"
     ]
    },
    {
     "ename": "Interpreter Error",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "Interpreter Error: "
     ]
    }
   ],
   "source": [
    "{\n",
    "    int a { 2 }, b { 3 };\n",
    "    const int* const p { &a }; // Nothing is modifiable\n",
    "    \n",
    "    p = &b; // COMPILATION ERROR\n",
    "    *p = 5; // COMPILATION ERROR \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "**IMPORTANT**: Even if declared `const`, the pointed value is\n",
    "not intrinsically constant. It just can't be\n",
    "modified through this precise pointer.\n",
    "If other variables reference the same memory area and \n",
    "are not constant, they are able to modify the value:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Value pointed by p (which doesn't allow value modification) is: 2\n",
      "Value pointed by p (and modified through p2) is: 10\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    int a { 2 }, b { 3 };\n",
    "    const int* p { &a }; // Address pointed to is modifiable, but not the underlying value.\n",
    "    \n",
    "    std::cout << \"Value pointed by p (which doesn't allow value modification) is: \" << *p << std::endl;\n",
    "    \n",
    "    int* p2 {&a}; // Pointer to the same memory area, but no constness here.\n",
    "    *p2 = 10;\n",
    "    \n",
    "    std::cout << \"Value pointed by p (and modified through p2) is: \" << *p << std::endl;    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On the other hand, pointers can't be used as a work-around to modify a constant value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[1minput_line_25:4:16: \u001b[0m\u001b[0;1;31merror: \u001b[0m\u001b[1mcannot initialize a variable of type 'int *' with an rvalue of type 'const int *'\u001b[0m\n",
      "    int* p_n { &n }; // COMPILATION ERROR\n",
      "\u001b[0;1;32m               ^~\n",
      "\u001b[0m"
     ]
    },
    {
     "ename": "Interpreter Error",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "Interpreter Error: "
     ]
    }
   ],
   "source": [
    "{\n",
    "    const int n { 3 };\n",
    "    int* p_n { &n }; // COMPILATION ERROR\n",
    "    const int* p_n2 { &n }; // OK\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Arrays\n",
    "\n",
    "The operator `[]` enables the creation of an array.\n",
    "\n",
    "**Beware:** In C++, the indexes of an array start at 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[1minput_line_30:7:32: \u001b[0m\u001b[0;1;35mwarning: \u001b[0m\u001b[1marray index 10 is past the end of the array (which contains 10 elements) [-Warray-bounds]\u001b[0m\n",
      "    std::cout << \"i[10] = \" << i[10] << \" (undefined behaviour: out of range. Warning identifies the issue)\" << std::endl ;\n",
      "\u001b[0;1;32m                               ^ ~~\n",
      "\u001b[0m\u001b[1minput_line_30:3:5: \u001b[0m\u001b[0;1;30mnote: \u001b[0marray 'i' declared here\u001b[0m\n",
      "    int i[10] ; // Array of 10 integers - not initialised properly!\n",
      "\u001b[0;1;32m    ^\n",
      "\u001b[0m"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "i[2] = 50428857 (may be gibberish: undefined behaviour due to lack of initialization!)\n",
      "i[10] = -2038759403 (undefined behaviour: out of range. Warning identifies the issue)\n",
      "x[1] = 2 (expected: 2.)\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    int i[10] ; // Array of 10 integers - not initialised properly!\n",
    "    double x[3] = { 1., 2., 3. } ; // Array of 3 reals.\n",
    "    \n",
    "    std::cout << \"i[2] = \" << i[2] << \" (may be gibberish: undefined behaviour due to lack of initialization!)\" << std::endl;\n",
    "    std::cout << \"i[10] = \" << i[10] << \" (undefined behaviour: out of range. Warning identifies the issue)\" << std::endl ;\n",
    "    std::cout << \"x[1] = \" << x[1] << \" (expected: 2.)\" << std::endl ;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multi-dimensional arrays are also possible:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "k[0][0] = 5 (expected: 5)\n",
      "k[1][2] = 9 (expected: 9)\n",
      "l[0][0] = 0 (expected: 0)\n",
      "l[1][2] = 0 (expected: 0)\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    int k[2][3] { { 5, 7, 0 }, { 3, 8, 9 }};\n",
    "    \n",
    "    std::cout << \"k[0][0] = \" << k[0][0] << \" (expected: 5)\" << std::endl;\n",
    "    std::cout << \"k[1][2] = \" << k[1][2] << \" (expected: 9)\" << std::endl;\n",
    "    \n",
    "    int l[2][3] {};  // default initialization of all elements of the array.\n",
    "    std::cout << \"l[0][0] = \" << l[0][0] << \" (expected: 0)\" << std::endl;\n",
    "    std::cout << \"l[1][2] = \" << l[1][2] << \" (expected: 0)\" << std::endl;\n",
    "    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**IMPORTANT**: so far we have considered only the case of _static_ arrays, for which the size is already known at compilation. We will deal with dynamic ones [later in this tutorial](/notebooks/1-ProceduralProgramming/5-DynamicAllocation.ipynb#Arrays-on-heap) (and also with the standard libraries [alternatives](/notebooks/5-UsefulConceptsAndSTL/3-Containers.ipynb) such as std::vector or std::array which are actually much more compelling)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Arrays and pointers\n",
    "\n",
    "The variable designating an array is similar to a constant pointer pointing to the beginning of the array. Increasing this pointer is like moving around in the array.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "10 (expected: 10)\n",
      "11 (expected: 11)\n",
      "20 (expected: 20)\n",
      "Value pointed by j is 20 (expected: 20)\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    int i[2] = { 10, 20 } ;\n",
    "    std::cout << *i << \" (expected: 10)\" << std::endl ;\n",
    "    std::cout << *i + 1 << \" (expected: 11)\" << std::endl ;\n",
    "    std::cout << *(i + 1) << \" (expected: 20)\" << std::endl ;\n",
    "    \n",
    "    int* j = i; // OK!\n",
    "    ++j; // OK!\n",
    "    std::cout << \"Value pointed by j is \" << *j << \" (expected: 20)\" << std::endl;\n",
    "}\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "© _CNRS 2016_ - _Inria 2018-2019_   \n",
    "_This notebook is an adaptation of a lecture prepared by David Chamont (CNRS) under the terms of the licence [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/)_  \n",
    "_The present version has been written by Sébastien Gilles and Vincent Rouvreau (Inria)_\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xeus-cling-cpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "-std=c++17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Table of contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
