## About this tutorial

This tutorial is heavily inspired from a C++ tutorial created by David Chamont (CNRS) that was given as a lecture with the help of Vincent Rouvreau (Inria) in 2016; latest version of this tutorial used as the basis of current one may be found [there](https://gitlab.inria.fr/FormationCpp/DebuterEnCpp).

Current version provides two major modifications:

* The tutorial is now in english.
* Jupyter notebooks using [Xeus-cling kernel](https://github.com/QuantStack/xeus-cling) are now used, thus enabling a sort of interpreted C++ which is rather helpful for teaching it (even if it is clearly not yet mature...)
 
I have rewritten entirely and modified heavily several chapters, but the backbone remains heavily indebted to David and Vincent and the TP is still very similar to the original one.

As the original tutorial, the present lecture is released under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/) licence.


## Goal of this tutorial

This is an introductory lecture to the modern way of programming C++; at the end of it you should:

* Understand the syntax and basic mechanisms of the C++ language in version 14/17.
* Know the different programming styles.
* Know of and be able to use the most useful part of the standard library.
* Be aware of many good programming practices in C++.


## How to run it?

As this tutorial relies heavily on [Xeus-cling kernel](https://github.com/QuantStack/xeus-cling), the only requirement is to install this environment on your machine.

Unfortunately, the support of this kernel is still experimental on Windows; I advise you to use any variant of Linux or macOS (for Windows 10 users you may [install Ubuntu in your Windows session](https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows); I haven't tried this myself).

Should the procedure described below not work at some point I invite you to check the link above, but at the time of this writing you need to:

* Install [miniconda3](https://conda.io/miniconda.html) environment (apparently using full-fledged anaconda may lead to conflict in dependancies).
* Create a new conda environment and activate it:

```
conda create -n cling
conda activate cling
```

* Install xeus-cling in this environment (and additional Jupyter extension to handle stuff like citations and table of contents).
```
conda install xeus-cling notebook jupyter_contrib_nbextensions -c QuantStack -c conda-forge
```

* Then you can run the notebook by going **into its root directory** (or internal links won't work...) in a terminal and typing:

```
jupyter notebook
```

__BEWARE__: I **strongly advise** to use a dedicated environment for the notebook: the ultimate goal is to use a real C++ environment for your project, and the anaconda environment hides the native C++ compiler on your machine.

