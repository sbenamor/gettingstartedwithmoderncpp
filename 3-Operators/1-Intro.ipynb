{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](/) - [Operators](/notebooks/3-Operators/0-main.ipynb) - [Introduction](/notebooks/3-Operators/1-Intro.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Motivation\" data-toc-modified-id=\"Motivation-1\">Motivation</a></span></li><li><span><a href=\"#Overloading-an-operator\" data-toc-modified-id=\"Overloading-an-operator-2\">Overloading an operator</a></span></li><li><span><a href=\"#Operator-between-different-types\" data-toc-modified-id=\"Operator-between-different-types-3\">Operator between different types</a></span></li><li><span><a href=\"#Limitations\" data-toc-modified-id=\"Limitations-4\">Limitations</a></span></li><li><span><a href=\"#Conversion-operators\" data-toc-modified-id=\"Conversion-operators-5\">Conversion operators</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation\n",
    "\n",
    "We've seen at length in the object programming part that classes are basically new types defined by the developer. However sometimes we would like to use exactly the same syntax as for the base type. Let's see for instance a basic class to handle tri-dimensional vectors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Vector\n",
    "{\n",
    "    // Friendship because `Add()` needs to access private members and no accessors were defined.\n",
    "    friend Vector Add(const Vector& v1, const Vector& v2);\n",
    "    \n",
    "    public :\n",
    "\n",
    "        Vector(double x, double y, double z);\n",
    "    \n",
    "        Vector() = default;\n",
    "    \n",
    "        void Print() const;\n",
    "\n",
    "    private :\n",
    "    \n",
    "        double x_ = 0.;\n",
    "        double y_ = 0.;\n",
    "        double z_ = 0.;\n",
    "}; "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector::Vector(double x, double y, double z)\n",
    ": x_(x),\n",
    "y_(y),\n",
    "z_(z)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector Add(const Vector& v1, const Vector& v2)\n",
    "{\n",
    "    Vector ret;\n",
    "    ret.x_ = v1.x_ + v2.x_;\n",
    "    ret.y_ = v1.y_ + v2.y_;\n",
    "    ret.z_ = v1.z_ + v2.z_;\n",
    "    \n",
    "    return ret;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void Vector::Print() const\n",
    "{\n",
    "    std::cout << \"(\" << x_ << \", \" << y_ << \", \" << z_ << \")\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Vector v1(3., 5., 7.);\n",
    "    Vector v2(7., 5., 3.);\n",
    "    \n",
    "    Vector v3 = Add(v1, v2);\n",
    "    v3.Print();\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the same with a _plain old data type_ is much more natural to write with no (apparent) method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    double x1 = 3.;\n",
    "    double x2 = 7.;\n",
    "    \n",
    "    double x3 = x1 + x2;    \n",
    "    std::cout << x3 << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "C++ provides the way to mimic this behaviour with **operator overloading**. This is a very powerful conceit, but also one that should be approached with some care...\n",
    "\n",
    "We will see the general way to define such an operator in this notebook and see in dedicated notebooks which are the ones specifically useful.\n",
    "\n",
    "## Overloading an operator\n",
    "\n",
    "To overload an operator, the syntax is just the keyword **operator** followed by the operator to overload."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Vector2\n",
    "{\n",
    "    public :\n",
    "\n",
    "        Vector2(double x, double y, double z);\n",
    "    \n",
    "        Vector2() = default;\n",
    "    \n",
    "        void Print() const;\n",
    "    \n",
    "        // I would rather put the definition outside but Xeus-cling doesn't seem to accept this.\n",
    "        Vector2 operator+(const Vector2& v) const\n",
    "        {\n",
    "            Vector2 ret;\n",
    "            ret.x_ = x_ + v.x_;\n",
    "            ret.y_ = y_ + v.y_;\n",
    "            ret.z_ = z_ + v.z_;\n",
    "    \n",
    "            return ret;\n",
    "        }\n",
    "\n",
    "    private :\n",
    "    \n",
    "        double x_ = 0.;\n",
    "        double y_ = 0.;\n",
    "        double z_ = 0.;\n",
    "}; "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector2::Vector2(double x, double y, double z)\n",
    ": x_(x),\n",
    "y_(y),\n",
    "z_(z)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void Vector2::Print() const\n",
    "{\n",
    "    std::cout << \"(\" << x_ << \", \" << y_ << \", \" << z_ << \")\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Vector2 v1(3., 5., 7.);\n",
    "    Vector2 v2(7., 5., 3.);\n",
    "    \n",
    "    Vector2 v3 = v1 + v2;\n",
    "    v3.Print();\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see in the definition of the operator+ that both `Vector2` added aren't symmetric: one is the data attribute while the other is the data attribute of an object given as an argument.\n",
    "\n",
    "As a side note, please remark the `operator+` implementation is able to reach the private data attributes of the argument `v`; this means the private status is set **at class level** and not at object level.\n",
    "\n",
    "\n",
    "It is actually possible to define the operator as a free function, thus providing a more symmetric implementation:\n",
    "\n",
    "**Xeus-cling issue**: cling doesn't accept operator definition outside of class; please use [@Coliru](https://coliru.stacked-crooked.com/a/626efa4fb6a02915):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Xeus-cling issue: doesn't compile!\n",
    "\n",
    "#include <iostream>\n",
    "\n",
    "class Vector3\n",
    "{\n",
    "    public :\n",
    "\n",
    "        Vector3(double x, double y, double z);\n",
    "    \n",
    "        Vector3() = default;\n",
    "    \n",
    "        void Print() const;\n",
    "    \n",
    "        friend Vector3 operator+(const Vector3& v1, const Vector3& v2);\n",
    "\n",
    "    private :\n",
    "    \n",
    "        double x_ = 0.;\n",
    "        double y_ = 0.;\n",
    "        double z_ = 0.;\n",
    "}; \n",
    "\n",
    "Vector3::Vector3(double x, double y, double z)\n",
    ": x_(x),\n",
    "y_(y),\n",
    "z_(z)\n",
    "{ }\n",
    "\n",
    "\n",
    "void Vector3::Print() const\n",
    "{\n",
    "    std::cout << \"(\" << x_ << \", \" << y_ << \", \" << z_ << \")\" << std::endl;\n",
    "}\n",
    "\n",
    "\n",
    "Vector3 operator+(const Vector3& v1, const Vector3& v2) \n",
    "{    \n",
    "    // Provides a symmetric implementation of operator +: both vectors are at the same level!    \n",
    "    Vector3 ret;\n",
    "    ret.x_ = v1.x_ + v2.x_;\n",
    "    ret.y_ = v1.y_ + v2.y_;\n",
    "    ret.z_ = v1.z_ + v2.z_;\n",
    "    \n",
    "    return ret;\n",
    "}\n",
    "\n",
    "\n",
    "int main(int argc, char** argv)\n",
    "{\n",
    "    Vector3 v1(3., 5., 7.);\n",
    "    Vector3 v2(7., 5., 3.);\n",
    "    \n",
    "    Vector3 v3 = v1 + v2;\n",
    "    v3.Print();\n",
    "    \n",
    "    return EXIT_SUCCESS;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operator between different types\n",
    "\n",
    "It is also possible to define an operator which acts upon two objects of different nature:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Vector4\n",
    "{\n",
    "    public :\n",
    "\n",
    "        Vector4(double x, double y, double z);\n",
    "    \n",
    "        Vector4() = default;\n",
    "    \n",
    "        void Print() const;\n",
    "    \n",
    "        // Defined in the class declaration due to Xeus-cling limitation.\n",
    "        Vector4 operator+(double value) const\n",
    "        {\n",
    "            Vector4 ret;\n",
    "            ret.x_ = x_ + value;\n",
    "            ret.y_ = y_ + value;\n",
    "            ret.z_ = z_ + value;\n",
    "    \n",
    "            return ret;\n",
    "        }\n",
    "\n",
    "    private :\n",
    "    \n",
    "        double x_ = 0.;\n",
    "        double y_ = 0.;\n",
    "        double z_ = 0.;\n",
    "}; "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void Vector4::Print() const\n",
    "{\n",
    "    std::cout << \"(\" << x_ << \", \" << y_ << \", \" << z_ << \")\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector4::Vector4(double x, double y, double z)\n",
    ": x_(x),\n",
    "y_(y),\n",
    "z_(z)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Vector4 vector(5., 3.2, -1.);\n",
    "    Vector4 vector_plus_5 = vector + 5.;\n",
    "    vector_plus_5.Print();\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, pay attention to the fact this operator is not commutative: when you call \n",
    "\n",
    "````\n",
    "Vector4 vector_plus_5 = vector + 5.;\n",
    "````\n",
    "\n",
    "it is indeed a shortcut to\n",
    "\n",
    "````\n",
    "Vector4 vector_plus_5 = vector.operator+(5.);\n",
    "````\n",
    "\n",
    "and the following won't compile:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Vector4 vector(5., 3.2, -1.);\n",
    "    Vector4 vector_plus_5 = 5. + vector; // COMPILATION ERROR!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want it to be possible, you have to define the operator with arguments in both orders; you therefore need to use out-of-class prototype of the function (can't show it currently due to Xeus-cling limitation).\n",
    "\n",
    "Of course, if you do so you should define one in way of the other:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Won't compile in Xeus-cling\n",
    "\n",
    "#include <cstdlib>\n",
    "#include <iostream>\n",
    "\n",
    "class Vector5\n",
    "{\n",
    "    public :\n",
    "\n",
    "        Vector5(double x, double y, double z);\n",
    "    \n",
    "        Vector5() = default;\n",
    "    \n",
    "        void Print() const;\n",
    "    \n",
    "        friend Vector5 operator+(const Vector5& v, double value);\n",
    "    \n",
    "        friend Vector5 operator+(double value, const Vector5& v);\n",
    "\n",
    "    private :\n",
    "    \n",
    "        double x_ = 0.;\n",
    "        double y_ = 0.;\n",
    "        double z_ = 0.;\n",
    "}; \n",
    "\n",
    "\n",
    "\n",
    "void Vector5::Print() const\n",
    "{\n",
    "    std::cout << \"(\" << x_ << \", \" << y_ << \", \" << z_ << \")\" << std::endl;\n",
    "}\n",
    "\n",
    "\n",
    "Vector5::Vector5(double x, double y, double z)\n",
    ": x_(x),\n",
    "y_(y),\n",
    "z_(z)\n",
    "{ }\n",
    "\n",
    "\n",
    "\n",
    "Vector5 operator+(const Vector5& v, double value)\n",
    "{\n",
    "    Vector5 ret;\n",
    "    \n",
    "    ret.x_ = v.x_ + value;\n",
    "    ret.y_ = v.y_ + value;\n",
    "    ret.z_ = v.z_ + value;\n",
    "    \n",
    "    return ret;\n",
    "}\n",
    "\n",
    "\n",
    "Vector5 operator+(double value, const Vector5& v)\n",
    "{\n",
    "    return v + value;\n",
    "}\n",
    "\n",
    "\n",
    "int main(int argc, char** argv)\n",
    "{\n",
    "    Vector5 vector(5., 3.2, -1.);\n",
    "    Vector5 vector_plus_5 = vector + 5.;\n",
    "    Vector5 vector_plus_5_commutated = 5. + vector;\n",
    "    \n",
    "    vector_plus_5.Print();\n",
    "    vector_plus_5_commutated.Print();\n",
    "    \n",
    "    return EXIT_SUCCESS;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Limitations\n",
    "\n",
    "You cannot change:\n",
    "* The number of operators arguments\n",
    "* The precedence rules (between `+` and `*` for instance)\n",
    "\n",
    "You can't _invent_ new operators, but only redefine operators in the following list (that might be incomplete: I learnt about `\"\"` operator very recently on [this blog post](https://www.fluentcpp.com/2016/12/08/strong-types-for-strong-interfaces/)):\n",
    "\n",
    "````\n",
    "+ - * / % ^ & | ~ ! \n",
    "= < > += -= *= /= %= ^= &=\n",
    "|= << >> >>= <<= == != <= >= &&\n",
    "|| ++ -- ->* , -> [] () new delete\n",
    "\"\"\n",
    "````\n",
    "\n",
    "(plus **conversion operators** - see next section).\n",
    "\n",
    "If not defined, some of them exist by default:\n",
    "\n",
    "````\n",
    "= \n",
    ". -> .* ->*\n",
    "new delete sizeof\n",
    "\n",
    "````\n",
    "\n",
    "Some can never be redefined:\n",
    "\n",
    "````\n",
    ": :: . .* ? ?: sizeof\n",
    "````\n",
    "\n",
    "\n",
    "## Conversion operators\n",
    "\n",
    "A conversion operator is a method of transforming an object into a given type. When the compiler needs to force the type of an object, implicitly or explicitly, it is this operator that will be called. A conversion operator is required for each type of conversion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "class Rational\n",
    " {\n",
    "  public :\n",
    "\n",
    "    Rational(int numerator, int denominator);\n",
    "\n",
    "    operator int() const;\n",
    "\n",
    "    operator double() const;\n",
    "\n",
    "  private :\n",
    "\n",
    "    int numerator_ = 0;\n",
    "    int denominator_ = 0;\n",
    "\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rational::Rational(int numerator, int denominator)\n",
    ": numerator_(numerator),\n",
    "denominator_(denominator)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rational::operator int() const\n",
    "{\n",
    "    return numerator_ / denominator_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rational::operator double() const\n",
    "{\n",
    "    return static_cast<double>(numerator_) / denominator_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Rational val_r(15, 7);\n",
    "    double val_d {val_r};\n",
    "    int val_i{val_r};\n",
    "    \n",
    "    std::cout << \"val as double: \" << val_d << std::endl ;\n",
    "    std::cout << \"val as integer: \" << val_i << std::endl ;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As for constructors, you might add the keyword `explicit` in the class declaration to ensure no implicit conversion occurs:\n",
    "\n",
    "````\n",
    "explicit operator int() const;\n",
    "explicit operator double() const;\n",
    "````"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "© _CNRS 2016_ - _Inria 2018-2019_   \n",
    "_This notebook is an adaptation of a lecture prepared by David Chamont (CNRS) under the terms of the licence [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/)_  \n",
    "_The present version has been written by Sébastien Gilles and Vincent Rouvreau (Inria)_"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xeus-cling-cpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "-std=c++17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
